import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import './listItem.dart';

class ListContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListItem(
          imagePath: 'assets/images/BostonLettuce.png',
          itemName: "Boston Lettuce",
          price: "1.10",
          unit: "piece",
        ),
        //SizedBox for precise spacing
        SizedBox(
          height: MediaQuery.of(context).size.height * 40 / 814,
        ),
        ListItem(
          imagePath: 'assets/images/PurpleCauliflower.png',
          itemName: "Purple Cauliflower",
          price: "1.85",
        ),
        //SizedBox for precise spacing
        SizedBox(
          height: MediaQuery.of(context).size.height * 40 / 814,
        ),
        ListItem(
          imagePath: 'assets/images/SavoyCabbage.png',
          itemName: "Savoy Cabbage",
          price: "1.45",
        ),
        //SizedBox for precise spacing
        SizedBox(
          height: MediaQuery.of(context).size.height * 40 / 814,
        ),
      ],
    );
  }
}
