import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListItem extends StatelessWidget {
  final String imagePath;
  final String itemName;
  final String price;
  final String unit;

  ListItem({
    @required this.imagePath,
    @required this.itemName,
    @required this.price,
    this.unit =
        "kg", //default argument because 2/3 items contain kg as their units
  });
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Container(
      margin: const EdgeInsets.only(left: 20.0),
      width: mediaQuery.size.width,
      child: Row(
        children: <Widget>[
          //A container for the leading image in the item
          Container(
            height: mediaQuery.size.height * 128 / 816,
            width: mediaQuery.size.width *
                .427, //giving 42.7% of the screen's width to the container as per the mockup

            //using ClipRRect for rounded corners on the image when BoxFit.fitHeight is used
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Image.asset(
                imagePath,
                fit: BoxFit.cover,
              ),
            ),
          ),
          //SizedBox for precise spacing
          SizedBox(
            width: mediaQuery.size.width *
                .048, // 20 pixels space in 414 pixels screeb equates to 4.8%, responsive deign
          ),
          //The container for the text alongside the image
          Container(
            width: (mediaQuery.size.width - 40) *
                (1 -
                    0.427 -
                    0.048), //rest of the unused space is allocated to this container
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Text Widget for the name of the item
                Text(
                  itemName,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).textTheme.title.color,
                  ),
                ),
                //sized box for precise spacing according to the mockup
                const SizedBox(
                  height: 12.0,
                ),
                //Row that contains the price information
                Row(
                  children: <Widget>[
                    //Text Widget that contains the price information
                    Text(
                      "$price ",
                      style: TextStyle(
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).textTheme.title.color,
                      ),
                    ),
                    Text(
                      "€ / $unit",
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  ],
                ),
                //Precise and responsive spacing using sizedbox and mediaQuery.
                SizedBox(
                  height: mediaQuery.size.height * 30 / 896,
                ),
                //Row that contains the two icon buttons
                Row(
                  children: <Widget>[
                    Container(
                      height: 40.0,
                      width: mediaQuery.size.width * 78.0 / 414,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          10.0,
                        ),
                        border: Border.all(
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                      child: IconButton(
                        onPressed: () {},
                        color: Theme.of(context).accentColor,
                        icon: Icon(
                          Icons.favorite_border,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: mediaQuery.size.width * 20.0 / 414,
                    ),
                    Container(
                      height: 40.0,
                      width: mediaQuery.size.width * 78.0 / 414,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(11, 206, 1, 1),
                        borderRadius: BorderRadius.circular(
                          10.0,
                        ),
                        border: Border.all(
                          style: BorderStyle.none,
                        ),
                      ),
                      child: IconButton(
                        onPressed: () {},
                        color: Colors.white,
                        icon: Icon(
                          CupertinoIcons.shopping_cart,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
