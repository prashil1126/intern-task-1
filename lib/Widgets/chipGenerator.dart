import 'package:flutter/material.dart';

class ChipGenerator extends StatefulWidget {
  final Widget label;
  final bool isSelected;
  ChipGenerator({@required this.label, this.isSelected = false});
  @override
  _ChipGeneratorState createState() => _ChipGeneratorState();
}

class _ChipGeneratorState extends State<ChipGenerator> {
  @override
  Widget build(BuildContext context) {
    //using InputChip for that effect of being able to click, even if not functional cuurently
    return InputChip(
      backgroundColor:
          widget.isSelected ? Color.fromRGBO(226, 203, 255, 1.0) : Colors.white,
      label: widget.label,
      onPressed: () {},
    );
  }
}
