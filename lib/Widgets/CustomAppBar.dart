import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size
      preferredSize; //required to make a custom appbar as the appbar is of type PreferredSizeWidget

  CustomAppBar()
      : preferredSize = Size.fromHeight(
            213.0); //required because of implementation of PreferredSizeWidget
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context); //to make the app responsive
    //safe area used so that the app does not render in the status bar
    return SafeArea(
      child: Container(
        margin: const EdgeInsets.only(left: 20.0),
        //using stack because the search bar appears on top of the image in the mockup
        child: Stack(
          children: <Widget>[
            //using align widget with stack for precise placement
            Align(
              alignment: Alignment.topRight,
              child: Image.asset(
                'assets/images/appbar_image.png',
                height: mediaQuery.size.height * 180 / 896,
              ),
            ),
            //using align widget with stack for precise placement
            Align(
              alignment: Alignment.centerLeft,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.arrow_back_ios),
                  SizedBox(
                    height: mediaQuery.size.height *
                        (23.0 / 816), //responsive spacing
                  ),
                  Text(
                    "Vegetables",
                    style: Theme.of(context).textTheme.title,
                  ),
                  //sized box for spacing
                  SizedBox(
                    height: mediaQuery.size.height *
                        (27.0 / 816), //responsive spacing
                  )
                ],
              ),
            ),
            //using align widget with stack for precise placement
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                margin: const EdgeInsets.only(right: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40.0),
                  color: Colors
                      .white, //white color used so that the image behind the container is hidden
                ),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search),
                    //the border when the user is editing the text field
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Color.fromRGBO(0, 23, 86, 1),
                          style: BorderStyle.solid,
                          width: 1.0),
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    //the border when the textfield is enabled
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Color.fromRGBO(217, 208, 227, 1),
                          style: BorderStyle.solid,
                          width: 1.0),
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    hintText: "Search",
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
