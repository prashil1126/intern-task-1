import 'package:flutter/material.dart';

import './chipGenerator.dart';

class ChipContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Container(
      padding: const EdgeInsets.only(left: 20.0),
      //using height constraint so a listview can be accomodated inside a column of the main.dart file
      height: mediaQuery.size.height * (120 / 896),
      //using listview to scroll to the chips that appear out of screen in the mockup
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  //Generate the chip from a custom widget using constructor
                  ChipGenerator(
                    label: Row(
                      children: <Widget>[
                        Icon(
                          Icons.check,
                          color: Color.fromRGBO(108, 14, 228, 1.0),
                        ),
                        Text(
                          "Cabbage and Lettuces (14)",
                          style: TextStyle(
                            color: Color.fromRGBO(108, 14, 228, 1.0),
                          ),
                        ),
                      ],
                    ),
                    isSelected: true,
                  ),
                  //SizedBox for spacing
                  SizedBox(
                    width: mediaQuery.size.width * (8 / 414),
                  ),
                  //Generate the chip from a custom widget using constructor
                  ChipGenerator(
                    label: Text(
                      "Cucumbers and Tomatoes (12)",
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  //Generate the chip from a custom widget using constructor
                  ChipGenerator(
                    label: Text(
                      "Onions and Garlic (8)",
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  ),
                  //SizedBox for spacing
                  SizedBox(
                    width: mediaQuery.size.width * (8 / 414),
                  ),
                  //Generate the chip from a custom widget using constructor
                  ChipGenerator(
                    label: Text(
                      "Peppers (7)",
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  ),
                  //SizedBox for spacing
                  SizedBox(
                    width: mediaQuery.size.width * (8 / 414),
                  ),
                  //Generate the chip from a custom widget using constructor
                  ChipGenerator(
                    label: Text(
                      "Potatoes and Carrots (4)",
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
