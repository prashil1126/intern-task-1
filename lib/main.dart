import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import './Widgets/listContainer.dart';
import './Widgets/CustomAppBar.dart';
import './Widgets/chipContainer.dart';

void main() {
  runApp(
    MaterialApp(
      home: InternApp(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color.fromRGBO(45, 12, 87, 1),
        accentColor: Color.fromRGBO(147, 113, 189, 1),
        scaffoldBackgroundColor: Color.fromRGBO(246, 245, 245, 1),
        textTheme: ThemeData.light().textTheme.copyWith(
              title: TextStyle(
                fontSize: 30.0,
                color: Color.fromRGBO(45, 12, 87, 1),
                fontWeight: FontWeight.bold,
              ),
              subtitle: TextStyle(
                fontSize: 14.0,
                color: Color.fromRGBO(139, 134, 168, 1),
              ),
            ),
      ),
    ),
  );
}

class InternApp extends StatefulWidget {
  @override
  _InternAppState createState() => _InternAppState();
}

class _InternAppState extends State<InternApp> {
  var _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(
        context); //for responsiveness on phones of different sizes
    return Scaffold(
      appBar: CustomAppBar(), //the custom AppBar
      body: Container(
        margin: const EdgeInsets.only(
          top: 46.0,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //Custom ChipContainerWidget
              ChipContainer(),
              //SizedBox for spacing
              SizedBox(
                height: mediaQuery.size.height * (48 / 896),
              ),
              //Container Containing the ListItems
              ListContainer(),
            ],
          ),
        ),
      ),
      //the bottom bar design
      bottomNavigationBar: Container(
        height: mediaQuery.size.height * (80 / 896),
        child: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons
                    .apps, //used this because the icon in the mockup is not a part of Icon class in android or ios
              ),
              title:
                  Container(), // because there is no text label on the bottom bar items and title cannot be left null
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.shopping_cart,
              ),
              title:
                  Container(), // because there is no text label on the bottom bar items and title cannot be left null
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person_outline,
              ),
              title:
                  Container(), // because there is no text label on the bottom bar items and title cannot be left null
            ),
          ],
          type: BottomNavigationBarType.fixed,
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          selectedItemColor: Color.fromRGBO(114, 3, 255, 1),
          unselectedItemColor: Color.fromRGBO(149, 134, 168, 1),
        ),
      ),
    );
  }
}
